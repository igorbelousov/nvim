return {
	"craftzdog/solarized-osaka.nvim",
	priority = 1000,
	transparent = true,
	config = function()
		require("solarized-osaka").setup({
			on_highlights = function(hl, c)
				hl.LspInlayHint = {
					bg = c.none,
					fg = c.base00,
				}
			end,
		})

		vim.cmd([[colorscheme solarized-osaka]])
	end,
}
