return {
	"folke/which-key.nvim",
	lazy = false,
	init = function()
		vim.o.timeout = true
		vim.o.timeoutlen = 300
	end,
	opts = {
		spec = {
			mode = { "n", "v" },
			{ "<leader>g", group = "Git" },
			{ "<leader>;", group = "Telescope" },
			{ "<leader>u", group = "Debug" },
			{ "<leader>T", group = "Tests" },
		},
		icons = {
			mappings = false,
		},
	},
	keys = {
		{
			"<leader>?",
			function()
				require("which-key").show({ global = true })
			end,
			desc = "Buffer Local Keymaps (which-key)",
		},
	},
}
