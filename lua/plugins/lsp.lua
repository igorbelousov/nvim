local lsp = require("functions.lsp")

local described = lsp.described

local opts = { noremap = true, silent = true }

local function setup_generic()
	local lspconfig = require("lspconfig")

	local generic_servers = {
		"rust_analyzer",
		"bashls",
		-- "vtsls",
		"templ",
		"emmet_ls",
		"prettier-d",
		-- "angularls",
	}
	for _, client in ipairs(generic_servers) do
		lspconfig[client].setup({
			on_attach = lsp.on_attach,
			capabilities = lsp.get_capabilities(),
		})
	end
end

local function setup_go()
	local lspconfig = require("lspconfig")

	local hints = vim.empty_dict()
	hints.assignVariableTypes = true
	hints.compositeLiteralFields = true
	hints.compositeLiteralTypes = true
	hints.constantValues = true
	hints.functionTypeParameters = true
	hints.parameterNames = true
	hints.rangeVariableTypes = true

	lspconfig.gopls.setup({
		on_attach = function(client, bufnr)
			client.server_capabilities.documentFormattingProvider = false
			lsp.on_attach(client, bufnr)
		end,
		settings = {
			gopls = {
				codelenses = {
					test = true,
					regenerate_cgo = true,
					gc_details = true,
					generate = true,
					run_govulncheck = true,
					tidy = true,
					upgrade_dependency = true,
					vendor = true,
				},
				gofumpt = true,
				completeUnimported = true,
				usePlaceholders = true,
				diagnosticsDelay = "100ms",
				staticcheck = true,
				directoryFilters = { "-.git", "-.vscode", "-.idea", "-.vscode-test", "-node_modules" },
				semanticTokens = true,
				hints = hints,
				analyses = {
					fieldalignment = true,
					unusedparams = true,
					framepointer = true,
					sigchanyzer = true,
					unreachable = true,
					stdversion = true,
					unusedwrite = true,
					unusedvariable = true,
					useany = true,
					nilness = true,
				},
			},
		},
		capabilities = lsp.get_capabilities(),
	})
end

local function setup_tsserver()
	local lspconfig = require("lspconfig")

	lspconfig.ts_ls.setup({
		root_dir = function(...)
			return require("lspconfig.util").root_pattern(".git")(...)
		end,
		on_attach = function(client, bufnr)
			client.server_capabilities.documentFormattingProvider = false
			lsp.on_attach(client, bufnr)
		end,
		single_file_support = false,
		settings = {
			typescript = {
				inlayHints = {
					includeInlayParameterNameHints = "literal",
					includeInlayParameterNameHintsWhenArgumentMatchesName = false,
					includeInlayFunctionParameterTypeHints = true,
					includeInlayVariableTypeHints = false,
					includeInlayPropertyDeclarationTypeHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayEnumMemberValueHints = true,
				},
			},

			javascript = {
				inlayHints = {
					includeInlayParameterNameHints = "all",
					includeInlayParameterNameHintsWhenArgumentMatchesName = false,
					includeInlayFunctionParameterTypeHints = true,
					includeInlayVariableTypeHints = true,
					includeInlayPropertyDeclarationTypeHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayEnumMemberValueHints = true,
				},
			},
		},
		capabilities = lsp.get_capabilities(),
	})
end

local function setup_eslint()
	local lspconfig = require("lspconfig")

	lspconfig.eslint.setup({
		on_attach = lsp.on_attach,
		filetypes = { "js", "ts" },
		capabilities = lsp.get_capabilities(),
		cmd = { "vscode-eslint-language-server", "--stdio" },
	})
end

local function setup_clangd()
	local lspconfig = require("lspconfig")

	lspconfig.clangd.setup({
		on_attach = lsp.on_attach,
		filetypes = { "c", "cpp" },
		capabilities = lsp.get_capabilities(),
	})
end

local function setup_lua()
	local lspconfig = require("lspconfig")

	lspconfig.lua_ls.setup({
		on_attach = lsp.on_attach,
		capabilities = lsp.get_capabilities(),
		settings = {
			Lua = {
				diagnostics = {
					globals = {
						"vim",
						"assert",
						"describe",
						"it",
						"before_each",
						"after_each",
						"pending",
						"clear",
						"G_P",
						"G_R",
					},
				},
				format = {
					enable = false,
				},
			},
		},
	})
end

local function setup_python()
	local lspconfig = require("lspconfig")

	lspconfig.pylsp.setup({
		on_attach = lsp.on_attach,
		capabilities = lsp.get_capabilities(),
		settings = {
			settings = {
				pylsp = {
					plugins = {
						black = { enabled = false },
						autopep8 = { enabled = false },
						yapf = { enabled = false },
						pylint = { enabled = false },
						pyflakes = { enabled = false },
						pycodestyle = { enabled = false },
						pylsp_mypy = { enabled = true },
						jedi_completion = { fuzzy = true },
						pyls_isort = { enabled = true },
						sort = { enabled = true },
					},
				},
			},
		},
	})
end

return {
	{
		"chrisgrieser/nvim-lsp-endhints",
		event = "LspAttach",
		keys = {
			{
				"<leader>lit",
				function()
					require("lsp-endhints").toggle()
				end,
				desc = "Toggle lsp end hints",
			},
		},
		opts = {}, -- required, even if empty
	},

	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"folke/neodev.nvim",
		},
		config = function()
			require("neodev").setup({
				override = function(root_dir, options)
					for _, plugin in ipairs(require("lazy").plugins()) do
						if plugin.dev and root_dir == plugin.dir then
							options.plugins = true
						end
					end
				end,
			})
			setup_generic()
			setup_tsserver()
			setup_lua()
			setup_python()
			setup_clangd()
			setup_go()
			setup_eslint()
			local signs = { Error = " ", Warn = " ", Hint = "💡", Info = " " }
			for type, icon in pairs(signs) do
				local hl = "DiagnosticSign" .. type
				vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
			end

			vim.lsp.handlers["textDocument/publishDiagnostics"] =
				vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
					underline = true,
					update_in_insert = false,
					virtual_text = { spacing = 4, prefix = "●" },
					severity_sort = true,
				})
		end,
	},
}
