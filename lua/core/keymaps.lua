local keymap = vim.keymap
vim.g.mapleader = " "

keymap.set("n", "x", '"_x')

-- Increment/decrement
keymap.set("n", "+", "<C-a>", { silent = true })
keymap.set("n", "-", "<C-x>", { silent = true })

--move select
keymap.set("v", "J", ":m '>+1<CR>gv=gv", { silent = true })
keymap.set("v", "K", ":m '<-2<CR>gv=gv", { silent = true })

--golang keys
keymap.set("n", "<leader>Gt", ":GoTagAdd", { desc = "Go Add Tags" })
keymap.set("n", "<leader>Gr", ":GoTagRm", { desc = "Go Clear Tag" })
keymap.set("n", "<leader>Gc", ":GoCmt<cr>", { desc = "Go Doc", silent = true })
keymap.set("n", "<leader>Gi", ":GoImpl<cr>", { desc = "Go Implementation", silent = true })

--vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
keymap.set("n", "<leader>v", ":vsplit<CR>", { silent = true, desc = "Vertical Split" })
keymap.set("n", "<leader>s", ":split<CR>", { silent = true, desc = "Horizontal Split" })

keymap.set("n", "<C-h>", ":nohlsearch<CR>", { desc = "Remove search highlight", silent = true })

keymap.set("n", "<C-;> ", ":TodoTelescope<CR> ")
