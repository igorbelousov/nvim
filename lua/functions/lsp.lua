local M = {}

---describes argument, useful in mapping when you have *prototype*-like mapping
---options.
---@param x table
---@param desc string
---@return table
function M.described(x, desc)
	return vim.tbl_extend("force", x, { desc = desc })
end

local described = M.described

function M.on_attach(client, bufnr)
	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	-- See `:help vim.lsp.*` for documentation on any of the below functions
	local bufopts = { noremap = true, silent = true, buffer = bufnr }
	local builtins = require("telescope.builtin")

	vim.keymap.set({ "n", "v" }, "<leader>a", vim.lsp.buf.code_action, described(bufopts, "Perform code action"))

	vim.keymap.set("n", "<leader>n", vim.diagnostic.goto_next, { desc = "Next diagnostics" })
	vim.keymap.set("n", "<leader>p", vim.diagnostic.goto_prev, { desc = "Previous diagnostics" })

	vim.keymap.set("n", "<leader>lr", "<cmd>LspRestart<cr>")

	vim.keymap.set("n", "gd", builtins.lsp_definitions, { desc = "Go to definition" })
	vim.keymap.set("n", "gD", builtins.lsp_type_definitions, { desc = "Go to type definition" })
	vim.keymap.set("n", "go", builtins.lsp_outgoing_calls, { desc = "Show outgoing calls " })
	vim.keymap.set("n", "gi", builtins.lsp_incoming_calls, { desc = "Show incomimng calls" })
	vim.keymap.set("n", "<leader>r", vim.lsp.buf.rename, { desc = "Lsp rename" })
	vim.keymap.set("n", "gI", builtins.lsp_implementations, { desc = "Show implementations" })

	vim.keymap.set("n", "K", vim.lsp.buf.hover, described(bufopts, "Show Documentation"))

	vim.keymap.set("n", "gs", builtins.lsp_document_symbols, described(bufopts, "telescope Show Document Symbols"))

	if client then
		if client.server_capabilities.codeLensProvider then
			vim.api.nvim_create_autocmd({ "BufEnter", "InsertLeave" }, {
				group = vim.api.nvim_create_augroup("CodeLenses", {}),
				pattern = {
					-- NOTE: Here I list filetype that I wanna use with codelens.
					-- commented lines seem to do nothing, but hav lens enabled
					"*.go",
					"*.mod",
					-- "*.py",
					-- "*.lua",
				},
				callback = function()
					vim.lsp.codelens.refresh({ bufnr = 0 })
				end,
			})
			vim.api.nvim_buf_set_keymap(
				bufnr,
				"n",
				"<leader>lR",
				"<Cmd>lua vim.lsp.codelens.refresh { bufnr = 0 }<CR>",
				{ silent = true }
			)
			vim.api.nvim_buf_set_keymap(
				bufnr,
				"n",
				"<leader>lc",
				"<Cmd>lua vim.lsp.codelens.run()<CR>",
				{ silent = true }
			)
		end
		if client.server_capabilities.inlayHintProvider and vim.lsp.inlay_hint then
			vim.keymap.set("n", "<leader>lih", function()
				vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
			end, described(bufopts, "Toggle Inlay Hints"))
		end
	end
end

function M.get_capabilities()
	return require("cmp_nvim_lsp").default_capabilities()
end

return M
